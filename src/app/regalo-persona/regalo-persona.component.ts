import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { RegaloPersona} from './../models/persona-regalo.model';

@Component({
  selector: 'app-regalo-persona',
  templateUrl: './regalo-persona.component.html',
  styleUrls: ['./regalo-persona.component.css']
})
export class RegaloPersonaComponent implements OnInit {
  @Input() regalo: RegaloPersona;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit(): void {
  }

}
