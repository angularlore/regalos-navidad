import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegaloPersonaComponent } from './regalo-persona.component';

describe('RegaloPersonaComponent', () => {
  let component: RegaloPersonaComponent;
  let fixture: ComponentFixture<RegaloPersonaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegaloPersonaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegaloPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
