import { Component, OnInit } from '@angular/core';
import { RegaloPersona} from './../models/persona-regalo.model';

@Component({
  selector: 'app-lista-regalos',
  templateUrl: './lista-regalos.component.html',
  styleUrls: ['./lista-regalos.component.css']
})
export class ListaRegalosComponent implements OnInit {
  regalos: RegaloPersona[];
  constructor() {
    this.regalos = [];
   }

  ngOnInit(): void {
  }

  enviarregalo(persona:string,regalo:string):boolean{
  this.regalos.push(new RegaloPersona(persona,regalo));
  console.log(this.regalos);
  return false;

  }

}
