import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegaloPersonaComponent } from './regalo-persona/regalo-persona.component';
import { ListaRegalosComponent } from './lista-regalos/lista-regalos.component';

@NgModule({
  declarations: [
    AppComponent,
    RegaloPersonaComponent,
    ListaRegalosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
